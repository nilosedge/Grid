// Fig. 10.31: Grid.java
// Demonstrating GridLayout.
import java.applet.Applet;
import java.awt.*;
import java.lang.*;
import java.awt.event.*;
import java.lang.String;


public class Grid extends Applet implements ActionListener {
   private TextField numberscreen;
   private Button b[];
private Panel p;
   private String names[] =
      { "7", "8", "9", "/", "4", "5","6", "*", "1", "2","3", "-", "0", ".","=", "+"};
   private boolean toggle = true;    

   public void init()
   {



      // set layout to grid layout
//setLayout( new BorderLayout());
setLayout( new GridLayout( 2, 1 ) );
	numberscreen = new TextField("0");
numberscreen.setEditable(false);
//add(numberscreen,BorderLayout.NORTH);
add(numberscreen);
p = new Panel();
     p.setLayout( new GridLayout( 4, 4 ) );

      // create and add buttons
      b = new Button[ names.length ];
      for (int i = 0; i < names.length; i++ ) {
         b[ i ] = new Button( names[ i ] );
         b[ i ].addActionListener( this );
         p.add( b[ i ] );
//add(p,BorderLayout.SOUTH);
add(p);
      }
   }

   public void actionPerformed( ActionEvent e )
   { 
numberscreen.setText(e.getActionCommand());
      validate();
   }
}
